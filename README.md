Example Project Includes 

-180146_ProcuderalGeneration_Cameron_Pheiffer
-Prefabs
-Saved Textures
-Scenes
-Scripts
Made By Cameron Pheiffer

-GUITABLE Package 
Made By Jeremy Quentin
https://assetstore.unity.com/packages/tools/gui/editor-gui-table-108795

-AeonicTexturePack 
-Clouds
-EffectsExamples
-SkyDome
-Terrain Assets
Made By Penny De Byl
https://www.udemy.com/course/procedural-terrain-generation-with-unity/

-Stylised Nature Package
Made By Mikael Gustafson
https://assetstore.unity.com/packages/3d/environments/stylized-nature-pack-37457

-Standard Assets
Made By Unity Technologies
https://assetstore.unity.com/packages/essentials/asset-packs/standard-assets-for-unity-2018-4-32351

The Tutorials followed to Generate the Terrain texture, width and height is by Penny De Byl
https://www.udemy.com/course/procedural-terrain-generation-with-unity/

This Package Does Not work Currently With URP.

1 Simply Drag in Procuderal Package and Import Import
2 Open Scene named Procuderal Assets Demo Scene for full preview of package.
3 Open New Scene and create Terrain and drop custom Terrain scripts on to it.
4 Drag in assets to be used while checking Demo Scenes Component settings for correct Settings hints.
5 Generate Objects from top catogories to bottom.

Tool Package Includes
-Scripts
-Demo Assets
-Demo Scene for hints

Process :
The process was designed to be able to generate a enviroment on a terrain that a developer can change live with the terrain tools in unity while updating the terrain constantly witht the procuderal generation script. It is a system that is suppost to generate a scene for a developer while
keeping the developer in control.

Purpose :
To Assemble a Enviroment on a terrainn and spawn custom prefabs on to it while giving the creator freedom to create.

Terrain Requirements:
The terrain requires the custom terrain script to change terrain.
The water and skydome are prefabs that can be dragged in and used to add detail.


